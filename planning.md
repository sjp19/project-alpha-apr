importing and formatting from notes

Feature 1
* [x] Fork and clone the starter project from Project Alpha 
* [x] Create a new virtual environment in the repository directory for the project
* [x] Activate the virtual environment
* [x] Upgrade pip// SKIPPED DUE TO ENVIRONMENT ERROR, jonathan said OK
* [x] Install django
  * [x] pip install django
* [x] Install black
  * [x] pip install black
* [x] Install flake8
  * [x] pip install flake8
* [x] Install djhtml
  * [x] pip install djhtml
* [x] Deactivate your virtual environment
* [x] Activate your virtual environment
* [x] Use pip freeze to generate a requirements.txt file
* [x] python -m unittest tests.test_feature_01
* [x] git push

Feature 2
* [x] Create a Django project named tracker so that the manage.py file is in the top directory
  * [x] don't forget the .
* [x] Create a Django app named accounts and install it in the tracker Django project in the INSTALLED_APPS list
  * [x] "accounts.apps.AccountsConfig"
* [x] Create a Django app named projects and install it in the tracker Django project in the INSTALLED_APPS list
  * [x] "projects.apps.ProjectsConfig"
* [x] Create a Django app named tasks and install it in the tracker Django project in the INSTALLED_APPS list
  * [x] "tasks.apps.TasksConfig"
* [x] Run the migrations
* [x] Create a super user
* [x] python manage.py test tests.test_feature_02
* [x] git push

Feature 3
* [x] This feature is for you to create a Project model in the projects Django app.
* [x] name	string	maximum length of 200 characters
* [x] description	string	no maximum length
* [x] members	many-to-many	Refers to the auth.User model, related name "projects"
* [x] Moreover the Project model should implicitly convert to a string with the __str__ method that is the value of the name property.
* [x] Make migrations and migrate your database.
* [x] python manage.py test tests.test_feature_03
* [x] git push

Feature 4
* [x] Register the Project model with the admin so that you can see it in the Django admin site.
  * [x] admin.site.register
* [x] python manage.py test tests.test_feature_04
* [x] git push

Feture 5
* [x] Create a view that will get all of the instances of the Project model 
* [x] and puts them in the context for the template
* [x] Register that view in the projects app for the path "" and the name "list_projects" 
  * [x] in a new file named projects/urls.py
* [x] Include the URL patterns from the projects app in the tracker project with the prefix "projects/"
* [x] Create a template for the list view that complies with the following specifications
    * [x] the fundamental five in it
    * [x] a main tag that contains
    * [x] div tag that contains
    * [x] an h1 tag with the content "My Projects"
    * [x] if there are no projects created, then
    * [x] a p tag with the content "You are not assigned to any projects"
    * [x] otherwise a table that has two columns,
    * [x] The first with the header "Name" and the rows with the names of the projects
    * [x] The second with the header "Number of tasks" and nothing in those rows because we don't yet have tasks
* [x] python manage.py test tests.test_feature_05
* [x] git push

Feature 6
* [x] In the tracker urls.py, use the RedirectView to redirect from "" to the name of path for the list view that you created in the previous feature. Register that path a name of "home".
* [x] python manage.py test tests.test_feature_06
* [x] git push

Feature 7
* [x] Make sure you have a super user created so you can login to test the feature.
* [x] Register the LoginView  in your accounts urls.py with the path "login/" and the name "login"
* [x] Include the URL patterns from the accounts app in the tracker project with the prefix "accounts/"
* [x] Create a templates directory under accounts
* [x] Create a registration directory under templates
* [x] Create an HTML template named login.html in the registration directory
* [x] Put a post form in the login.html and any other required HTML or template inheritance stuff that you need to make it a valid Web page with the fundamental five (see specifications below)
  * [x] the fundamental five in it
  * [x] a main tag that contains
  * [x] div tag that contains
  * [x] an h1 tag with the content "Login"
  * [x] a form tag with method "post" that contains any kind of HTML structures but must include
  * [x] an input tag with type "text" and name "username"
  * [x] an input tag with type "password" and name "password"
  * [x] a button with the content "Login"
* [x] In the tracker settings.py file, create and set the variable LOGIN_REDIRECT_URL to the value "home" which will redirect us to the path (not yet created) with the name "home"
* [x] python manage.py test tests.test_feature_07
* [x] git push

Feature 8
* [x] Protect the list view for the Project model so that only a person that has logged in can access it
  * [x] mixin
* [x] Change the queryset of the view to filter the Project objects where members equals the logged in user
  * [x] get_queryset
* [x] python manage.py test tests.test_feature_08
* [x] git push

Feature 9
* [x] In the accounts/urls.py file,
* [x] Import the LogoutView from the same module that you imported the LoginView from
* [x] Register that view in your urlpatterns list with the path "logout/" and the name "logout"
* [x] In the tracker settings.py file, create and set the variable LOGOUT_REDIRECT_URL to the value "login" which will redirect the logout view to the login page
* [x] python manage.py test tests.test_feature_09
* [x] git push

Feature 10
* [x] You need to create a function view to handle showing the sign-up form, and handle its submission. This is a view, so you should create it in the file in the accounts directory that should hold the views.
* [x] You'll need to import the UserCreationForm from the built-in auth forms 
  * [x] from django.contrib.auth.forms import UserCreationForm
* [x] You'll need to use the special create_user  method to create a new user account from their username and password
  * [x] User.objects.create_user
* [x] You'll need to use the login  function that logs an account in
  * [x] login(request, user)
* [x] After you have created the user, redirect the browser to the path registered with the name "home"
* [x] Create an HTML template named signup.html in the registration directory
* [x] Put a post form in the signup.html and any other required HTML or template inheritance stuff that you need to make it a valid Web page with the fundamental five (see specifications below)
  * [x] the fundamental five in it
  * [x] a main tag that contains
  * [x] div tag that contains
  * [x] an h1 tag with the content "Signup"
  * [x] a form tag with method "post" that contains any kind of HTML structures but must include
  * [x] an input tag with type "text" and name "username"
  * [x] an input tag with type "password" and name "password1"
  * [x] an input tag with type "password" and name "password2"
  * [x] a button with the content "Signup"
* [x] python manage.py test tests.test_feature_10
* [x] git push

Feature 11
* [x] The Task model should have the following attributes.
* [x] name	string	maximum length of 200 characters
* [x] start_date	date-time	
* [x] due_date	date-time	
* [x] is_completed	Boolean	default value should be False
* [x] project	foreign key	Refers to the projects.Project model, related name "tasks", on delete cascade
* [x] assignee	foreign key	Refers to the auth.User model, null is True, related name "tasks", on delete set null
* [x] Moreover the Task model should implicitly convert to a string with the __str__ method that is the value of the name property.
* [x] Make migrations and migrate your database.
* [x] python manage.py test tests.test_feature_11
* [x] git push

Feature 12
* [x] Register the Task model with the admin so that you can see it in the Django admin site.
* [x] python manage.py test tests.test_feature_12
* [x] git push

Feature 13
* [x] Create a view that shows the details of a particular project
* [x] A user must be logged in to see the view
* [x] In the projects urls.py file, register the view with the path "<int:pk>/" and the name "show_project"
* [x] Create a template to show the details of the project and a table of its tasks
* [x] Update the list template to show the number of tasks for a project
* [x] Update the list template to have a link from the project name to the detail view for that project
  * [x] the fundamental five in it
  * [x] a main tag that contains
  * [x] div tag that contains
  * [x] an h1 tag with the project's name as its content
  * [x] a p tag with the project's description in it
  * [x] an h2 tag that has the content "Tasks"
  * [x] if the project has tasks, then
  * [x] a table that contains five columns with the headers "Name", "Assignee", "Start date", "Due date", and "Is completed" with rows for each task in the project
  * [x] otherwise
  * [x] a p tag with the content "This project has no tasks"
* [x] python manage.py test tests.test_feature_13
* [x] git push

Feature 14
* [x] Create a create view for the Project model that will show the name, description, and members properties in the form and handle the form submission to create a new Project
* [x] A person must be logged in to see the view
* [x] If the project is successfully created, it should redirect to the detail page for that project
* [x] Register that view for the path "create/" in the projects urls.py and the name "create_project"
* [x] Create an HTML template that shows the form to create a new Project (see the template specifications below)
    * [x] the fundamental five in it
    * [x] a main tag that contains
    * [x] div tag that contains
    * [x] an h1 tag with the content "Create a new project"
    * [x] a form tag with method "post" that contains any kind of HTML structures but must include
    * [x] an input tag with type "text" and name "name"
    * [x] a textarea tag with the name "description"
    * [x] a select tag with name "members"
    * [x] a button with the content "Create"
* [x] Add a link to the list view for the Project that navigates to the new create view
* [x] python manage.py test tests.test_feature_14
* [x] git push

Feature 15
* [x] Create a view that will show a form to create an instance of the Task model for all properties except the is_completed field
* [x] The view must only be accessible by people that are logged in
* [x] When the view successfully handles the form submission, have it redirect to the detail page of the task's project
* [x] Register that view in the tasks app for the path "create/" and the name "create_task" in a new file named tasks/urls.py
* [x] Include the URL patterns from the tasks app in the tracker project with the prefix "tasks/"
* [x] Create a template for the create view that complies with the following specifications
  * [x] the fundamental five in it
  * [x] a main tag that contains
  * [x] div tag that contains
  * [x] an h1 tag with the content "Create a new task"
  * [x] a form tag with method "post" that contains any kind of HTML structures but must include
  * [x] an input tag with type "text" and name "name"
  * [x] an input tag with type "text" and name "start_date"
  * [x] an input tag with type "text" and name "due_date"
  * [x] a select tag with name "projects"
  * [x] a select tag with name "assignee"
  * [x] a button tag with the content "Create"
* [x] Add a link to create a task from the project detail page that complies with the following specifications
  * [x] a p tag that contains
  * [x] an a tag with an href attribute that points to the "create_task" path with the content "Create a new task"
* [x] python manage.py test tests.test_feature_15
* [x] git push

Feature 16
* [x] Create a list view for the Task model with the objects filtered so that the person only sees the tasks assigned to them by filtering with the assignee equal to the currently logged in user
* [x] The view must only be accessible by people that are logged in
* [x] Register that view in the tasks app for the path "mine/" and the name "show_my_tasks" in the tasks urls.py file
* [x] Create an HTML template that conforms with the following specification
  * [x] the fundamental five in it
  * [x] a main tag that contains
  * [x] div tag that contains
  * [x] an h1 tag with the content "My Tasks"
  * [x] if there are tasks assigned to the person
  * [x] a table with the headers "Name", "Start date", "End date", "Is completed" and a row for each task that is assigned to the logged in person
  * [x] In the last column, if the task is completed, it should show the word "Done", otherwise, it should show nothing
  * [x] otherwise
  * [x] a p tag with the content "You have no tasks"
* [x] python manage.py test tests.test_feature_16
* [x] git push

Feature 17
* [x] Create an update view for the Task model that only is concerned with the is_completed field
* [x] When the view successfully handles a submission, it should redirect to the "show_my_tasks" URL path, that is, it should redirect to the "My Tasks" view (success_url property on a view class)
* [x] Register that view in the tasks app for the path "<int:pk>/complete/" and the name "complete_task" in the tasks urls.py file
* [x] You do not need to make a template for this view
* [x] Modify the "My Tasks" view to comply with the template specification
* [x] In the table cell that holds the status for each task in the "My Tasks" view, if the value of is_completed is False, then show this form.
```
<form method="post" action="{% url 'complete_task' task.id %}">
  {% csrf_token %}
  <input type="hidden" name="is_completed" value="True">
  <button>Complete</button>
</form>    
```
* [x] When the person clicks that button, the task's is_completed status should be updated to True and, then, the form disappears
* [x] python manage.py test tests.test_feature_17
* [x] git push

Feature 18
* [x] Install the django-markdownify package using pip (the first pip command in the instructions) and put it into the INSTALLED_APPS in the tracker settings.py according to the Installation  instructions block
* [x] Also, in the tracker settings.py file, add the configuration setting to disable sanitation 
* [x] In your the template for the Project detail view, load the markdownify template library as shown in the Usage  section
* [x] Replace the p tag and {{ project.description }} in the Project detail view with this code:
```
{{ project.description|markdownify }}
```
* [x] Use pip freeze to update your requirements.txt file
* [x] python manage.py test tests.test_feature_18
* [x] git push

Feature 19
* [x] In this feature, you'll add some navigation to the Web application.
* [x] a header tag as the first child of the body tag before the main tag that contains
* [x] a nav tag that contains
* [x] a ul tag that contains
* [x] if the person is logged in,
* [x] an li tag that contains
* [x] an a tag with an href to the "show_my_tasks" path with the content "My tasks"
* [x] an li tag that contains
* [x] an a tag with an href to the "list_projects" path with the content "My projects"
* [x] an li tag that contains
* [x] an a tag with an href to the "logout" path with the content "Logout"
* [x] otherwise
* [x] an li tag that contains
* [x] an a tag with an href to the "login" path with the content "Login"
* [x] an li tag that contains
* [x] an a tag with an href to the "signup" path with the content "Signup"
* [x] python manage.py test tests.test_feature_19
* [x] git push

FINISH
* [x] CHECK YOUR IMPORTS!!!!!!!!
* [x] RUN FLAKE8
* [x] RUN BLACK
* [x] FORMAT YOUR HTML USING DJHTML