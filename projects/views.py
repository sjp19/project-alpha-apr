from .models import Project
from tasks.models import Task
from django.views.generic.list import ListView
from django.views.generic.edit import CreateView
from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic.detail import DetailView
from django.urls import reverse_lazy

# Create your views here.


class ProjectListView(LoginRequiredMixin, ListView):
    model = Project
    template_name = "project/list.html"
    context_object_name = "project_listview"

    def get_queryset(self):
        return Project.objects.filter(members=self.request.user)


class ProjectDetailView(LoginRequiredMixin, DetailView):
    model = Project
    template_name = "project/details.html"
    context_object_name = "project_details"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["project_task"] = Task.objects.all()
        return context


class ProjectCreateView(LoginRequiredMixin, CreateView):
    model = Project
    template_name = "project/create.html"
    context_object_name = "project_create"
    fields = ["name", "description", "members"]

    def get_success_url(self):
        return reverse_lazy("show_project", kwargs={"pk": self.object.pk})
